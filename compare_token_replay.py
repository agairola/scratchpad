import sys
import importlib
sys.path.append('/root/abhijit/analytics-core')
from pm4py.objects.log.importer.xes import factory as xes_factory
from pm4py.objects.log.log import EventLog,Event,Trace
from pm4py.objects.conversion.log.versions import to_event_log
from pm4py.algo.conformance.tokenreplay import factory as tokenreplay
from pm4py.util.constants import PARAMETER_CONSTANT_TIMESTAMP_KEY,PARAMETER_CONSTANT_TIMESTAMP_KEY,PARAMETER_CONSTANT_ACTIVITY_KEY ,PARAMETER_CONSTANT_CASEID_KEY,PARAMETER_CONSTANT_START_TIMESTAMP_KEY,PARAMETER_CONSTANT_RESOURCE_KEY,PARAMETER_CONSTANT_TRANSITION_KEY
import pandas as pd
import math 
import multiprocessing as mp
import pytest
from itertools import chain
import numpy as np
from pm4py.util import constants
parameters = {
constants.PARAMETER_CONSTANT_TIMESTAMP_KEY: 'time:timestamp',
constants.PARAMETER_CONSTANT_ACTIVITY_KEY: 'concept:name',
constants.PARAMETER_CONSTANT_CASEID_KEY: 'case:concept:name',
constants.PARAMETER_CONSTANT_START_TIMESTAMP_KEY: None,
constants.PARAMETER_CONSTANT_TRANSITION_KEY: None,
constants.PARAMETER_CONSTANT_ATTRIBUTE_KEY: 'concept:name'}

from pme_source.Conformance import variant_selector
import time

print('reading in the csv..')
df = pd.read_csv('/root/abhijit/log43_corrected_no_index.csv')
df['time:timestamp'] = pd.to_datetime(df['time:timestamp'],utc = True).dt.tz_localize(None)
print('starting conversion...')
t = time.time()
log = to_event_log.apply(df,parameters)

# print('reading log...')
# t = time.time()
# log = xes_factory.apply('/root/abhijit/Desktop/pm4py-source/tests/input_data/RoadTrafficFine.xes')
# print(f'read in {time.time() - t} seconds')

print('starting wrapper_variants_count...')
t = time.time()
variants = variant_selector.wrapper_variants_count(log,parameters = parameters)
print(f'wrapper_variants_count finished in {time.time() -t } seconds')
var = [variant_dict['variant'] for variant_dict in variants['variants_count']]
t = time.time()
res = variant_selector.net_and_activities_from_variants(log,var[0:2],params = parameters)
input_time = time.time() - t
net = res['net']
initial_marking = res['initial_marking']
final_marking = res['final_marking']
#unwanted_activities = optimized_conformance_stats.unwanted_activities_from_filtered_log(log,net,initial_marking,final_marking)
print(f'net_and_activities_from_variant finished in {time.time() - t} seconds')
var_list = [[variant_dict['variant'],variant_dict['count']] for variant_dict in variants['variants_count']]


def chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]
    
    
def optimized_token_replay(var_list,net,initial_marking,final_marking,parameters):
    tmp = []
    result = []
    print('converting var_list to log')
    t = time.time()
    new_log = EventLog()
    #var_list = var_list[:231]
    for var_item in var_list:
        variant = var_item[0].split(',')
        tmp.append(len(variant))
        trace = Trace()
        for activ in variant:
            event = Event({parameters[PARAMETER_CONSTANT_ACTIVITY_KEY]: activ})
            trace.append(event)
        new_log.append(trace)
    
    print(f'conversion took {time.time() - t} seconds')
    print(f'length of new_log : {len(new_log)}')
    print(f'average length of trace in new_log : {np.mean(tmp)}')
    
    n = math.ceil(len(var_list)/mp.cpu_count())
    variants_list_split = list(chunks(new_log, n))
    
    with mp.Pool() as p:
        args = [(x,net,initial_marking,final_marking) for x in variants_list_split]
        result = list(p.starmap(tokenreplay.apply,args))

    return list(chain(*result))


print(f'len of log : {len(log)}')
print('starting regular token replay... ')
t = time.time()
reg_tr = tokenreplay.apply(log,net,initial_marking,final_marking,parameters = parameters)
print(f'finished regular token replay in {time.time() - t} seconds')

print('starting optimized token replay...')
t = time.time()
opt_tr = optimized_token_replay(var_list,net,initial_marking,final_marking,parameters)
print(f'finished optimized token replay in {time.time() - t} seconds')

#assert sum([1 if reg_tr['trace_is_fit'] else 0]) == sum([1 if opt_tr['trace_is_fit'] else 0])
# assert np.array_equal(sorted([1 if x['trace_fitness'] > 0.85 else 0 for x in reg_tr]),sorted([1 if x['trace_fitness'] > 0.85 else 0 for x in opt_tr]))