from itertools import chain
from pm4py.util.constants import (
    PARAMETER_CONSTANT_ACTIVITY_KEY,
    PARAMETER_CONSTANT_CASEID_KEY,
    PARAMETER_CONSTANT_TIMESTAMP_KEY
)

def diff_in_step_count(df, payload):
    activity_key = payload['parameters'][PARAMETER_CONSTANT_ACTIVITY_KEY]
    timestamp_key = payload['parameters'][PARAMETER_CONSTANT_TIMESTAMP_KEY]
    caseid_key = payload['parameters'][PARAMETER_CONSTANT_CASEID_KEY]
    df = df[[caseid_key, timestamp_key, activity_key]]
    tmp = df[df[caseid_key].isin(
        payload['caseids_containing_unwanted_activities']
        )]
    n_unwanted_activities = tmp[caseid_key].nunique()
    sum_steps_unwanted_activities = tmp.groupby(caseid_key) \
        .size().sum()
    
    tmp2 = df[~df[caseid_key].isin(
        payload['caseids_containing_unwanted_activities'])]
    n_no_unwanted_activities = tmp2[caseid_key].nunique()
    sum_steps_no_unwanted_activities = tmp.groupby(caseid_key) \
        .size().sum()
    
    if 'activity' in payload:
        return({'sum_steps_unwanted_activities': sum_steps_unwanted_activities,
            'n_unwanted_activities': n_unwanted_activities,
            'sum_no_unwanted_activities': sum_steps_no_unwanted_activities,
            'n_no_unwanted_activities': n_no_unwanted_activities,
            'activity': payload['activity']})
    else:
        return({'sum_steps_unwanted_activities': sum_steps_unwanted_activities,
            'n_unwanted_activities': n_unwanted_activities,
            'sum_no_unwanted_activities': sum_steps_no_unwanted_activities,
            'n_no_unwanted_activities': n_no_unwanted_activities})

def diff_in_step_count_agg(results):
    sum_un_act_lst = [None] * len(results)
    n_unwanted_act_lst = [None] * len(results)
    sum_no_un_act_lst = [None] * len(results)
    n_no_unwanted_act_lst = [None] * len(results)
    for i, res in enumerate(results):
        sum_un_act_lst[i] = res['sum_steps_unwanted_activities']
        n_unwanted_act_lst[i] = res['n_unwanted_activities']
        sum_no_un_act_lst[i] = res['sum_no_unwanted_activities']
        n_no_unwanted_act_lst[i] = res['n_no_unwanted_activities']
    
    avg_steps_has_unwanted = sum(sum_un_act_lst)/sum(n_unwanted_act_lst) if \
        sum(n_unwanted_act_lst) > 0 else 0
    avg_steps_no_unwanted = sum(sum_no_un_act_lst)/sum(n_no_unwanted_act_lst) if \
        sum(n_no_unwanted_act_lst) > 0 else 0
    change_in_mean_steps = avg_steps_no_unwanted - avg_steps_has_unwanted
    
    if 'activity' in results :
        return({'avg_steps_has_unwanted': avg_steps_has_unwanted,
            'avg_steps_no_unwanted': avg_steps_no_unwanted,
            'change_in_mean_steps': change_in_mean_steps,
            'activity': results['activity']})
    else:
        return({'avg_steps_has_unwanted': avg_steps_has_unwanted,
        'avg_steps_no_unwanted': avg_steps_no_unwanted,
        'change_in_mean_steps': change_in_mean_steps})
    

def diff_in_throughput_time(df,payload):
    activity_key = payload['parameters'][PARAMETER_CONSTANT_ACTIVITY_KEY]
    timestamp_key = payload['parameters'][PARAMETER_CONSTANT_TIMESTAMP_KEY]
    caseid_key = payload['parameters'][PARAMETER_CONSTANT_CASEID_KEY]
    caseids_containing_unwanted_activities = payload['caseids_containing_unwanted_activities']
    time_dict = payload['time_dict']
    time_metric = payload['time_metric']
    df = df[[caseid_key, timestamp_key, activity_key]]
    
    tmp1 =  df[df[caseid_key].isin(
        caseids_containing_unwanted_activities)]
    
    n_unwanted_activities = tmp1[caseid_key].nunique()
    time_unwanted_activities = tmp1\
                                        .groupby(caseid_key)[timestamp_key]\
                                        .agg(['first','last'])
    sum_time_unwanted_activities = (
        time_unwanted_activities['last'] - \
    time_unwanted_activities['first']).astype('timedelta64['
                                                     's]').sum()
    
    tmp2 = df[~df[caseid_key].isin(
        caseids_containing_unwanted_activities)]
    
    n_no_unwanted_activities = tmp2[caseid_key].nunique()
    time_no_unwanted_activities = tmp2\
                                    .groupby(caseid_key)[timestamp_key]\
                                    .agg(['first','last'])
    sum_time_no_unwanted_activities = \
        (time_no_unwanted_activities['last'] - \
        time_no_unwanted_activities['first']).astype(
            'timedelta64[s]').sum()
    
    if 'activity' in payload:
        return( {'n_unwanted_activities': n_unwanted_activities,
                'sum_time_unwanted_activities': sum_time_unwanted_activities/time_dict[time_metric],
                'n_no_unwanted_activities': n_no_unwanted_activities,
                'sum_time_no_unwanted_activities': sum_time_no_unwanted_activities/time_dict[time_metric],
                'activity': payload['activity']})
    else:
        return( {'n_unwanted_activities': n_unwanted_activities,
                'sum_time_unwanted_activities': sum_time_unwanted_activities/time_dict[time_metric],
                'n_no_unwanted_activities': n_no_unwanted_activities,
                'sum_time_no_unwanted_activities': sum_time_no_unwanted_activities/time_dict[time_metric]})
        
def diff_in_throughput_time_agg(results):
    sum_un_act_lst = [None] * len(results)
    n_unwanted_act_lst = [None] * len(results)
    sum_no_un_act_lst = [None] * len(results)
    n_no_unwanted_act_lst = [None] * len(results)
    for i, res in enumerate(results):
        sum_un_act_lst[i] = res['sum_time_unwanted_activities']
        n_unwanted_act_lst[i] = res['n_unwanted_activities']
        sum_no_un_act_lst[i] = res['sum_time_no_unwanted_activities']
        n_no_unwanted_act_lst[i] = res['n_no_unwanted_activities']
    
    throughput_time_has_unwanted = sum(sum_un_act_lst)/sum(n_unwanted_act_lst) if sum(n_unwanted_act_lst) > 0 else 0
    throughput_time_no_unwanted = sum(sum_no_un_act_lst)/sum(n_no_unwanted_act_lst) if sum(n_no_unwanted_act_lst) > 0 else 0
    change_in_throughput_time = throughput_time_no_unwanted - throughput_time_has_unwanted
    
    if 'activity' in results:
        return({'throughput_time_has_unwanted': throughput_time_has_unwanted,
            'throughput_time_no_unwanted': throughput_time_no_unwanted,
            'change_in_throughput_time': change_in_throughput_time,
            'activity' : results['activity']}) 
    else:
        return({'throughput_time_has_unwanted': throughput_time_has_unwanted,
            'throughput_time_no_unwanted': throughput_time_no_unwanted,
            'change_in_throughput_time': change_in_throughput_time}) 

def caseids_containing_unwanted_activities(df,payload):
    activity_key = payload['parameters'][PARAMETER_CONSTANT_ACTIVITY_KEY]
    caseid_key = payload['parameters'][PARAMETER_CONSTANT_CASEID_KEY]
    un_act = payload['unwanted_activities']
    un_act = un_act if isinstance(un_act,list) else [un_act]
    caseids_containing_unwanted_activities = df[df[activity_key].\
        str.contains('|'.join(un_act))][caseid_key].unique()
    if len(un_act) == 1:
        return {'activity': un_act[0],'caseids_containing_unwanted_activities':
            caseids_containing_unwanted_activities}
    else:
        return {'caseids_containing_unwanted_activities':
        caseids_containing_unwanted_activities}

def caseids_containing_unwanted_activities_agg(results):
    res = []
    for r in results:
        res.append(r['caseids_containing_unwanted_activities'])
    if 'activity' in results : 
        return {'activity' : results['activity'],'caseids_containing_unwanted_activities': list(set(chain(*res)))}
    else:
        return {'caseids_containing_unwanted_activities': list(set(chain(*res)))}

    

def n_unique_caseids(df,payload):
    caseid_key = payload['parameter'][PARAMETER_CONSTANT_CASEID_KEY]
    return df[caseid_key].nunique()
def n_unique_caseids_agg(results):
    return sum(results)

# def violation_stats(df,payload):

#     activity = payload['activity']
#     caseids_containing_unwanted_activities = payload['caseids_containing_unwanted_activities']
#     n_unique_caseids = payload['n_unique_caseids']
